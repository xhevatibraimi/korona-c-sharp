﻿using System;

namespace Example05.ConsoleReading
{
    class Program
    {
        static void Main(string[] args)
        {
            // state of the algorithm
            string firstName;
            string lastName;
            string age;
            
            // input of the algorithm
            Console.WriteLine("type first name");
            firstName = Console.ReadLine();

            Console.WriteLine("type lst name");
            lastName = Console.ReadLine();

            Console.WriteLine("type age");
            age = Console.ReadLine();

            // the algorithm
            string result = $"my name is {firstName}, my lastname is {lastName}, my age is {age}";

            // output of the algorithm
            Console.WriteLine(result);
        }
    }
}
