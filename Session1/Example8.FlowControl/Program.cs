﻿using System;

namespace Example07.FlowControl
{
    class Program
    {
        static void Main(string[] args)
        {
            // input
            string input = Console.ReadLine();

            // the algorithm
            if (input == "start")
            {
                // output
                Console.WriteLine("program has started");
            }
            else if (input == "stop")
            {
                // output
                Console.WriteLine("program has stopped");
            }
            else
            {
                // output
                Console.WriteLine("you have entered an invalid command");
            }
        }
    }
}
