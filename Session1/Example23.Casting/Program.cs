﻿using System;

namespace Example23.Casting
{
    class Program
    {
        static void Main(string[] args)
        {
            //double doubleNumber = 4.1;
            //int integerNumber = (int)doubleNumber;
            //Console.WriteLine(integerNumber);
            //(-2B)     (0)       (2B)         (4B)
            //------------------------          |
            //           ------------------------
            //0 -> max -> min -> 0 -> 1 2 3 4 
            uint unisgnedInteger32bit = uint.MaxValue;
            int int32bit = (int)(unisgnedInteger32bit + 1);
            Console.WriteLine(int32bit);
        }
    }
}
