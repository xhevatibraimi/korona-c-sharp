﻿using System;

namespace Example12.DoWhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 10;
            do
            {
                Console.WriteLine(number);
                number++;
            }
            while (number <= 15);
        }
    }
}
