﻿using System;

namespace Example09.FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            //F9 is a shortcut for breakpoint

            // The fizz buzz problem

            // 3 && 5 => "fizzbuzz"
            // 3      => "fizz"
            // 5      => "buzz"
            // x      => "kar"
            
            // input
            string input = Console.ReadLine();
            int number = int.Parse(input);

            // the algorithm
            if (number % 3 == 0 && number % 5 == 0)
            {
                Console.WriteLine("fizzbuzz");
            }
            else if (number % 5 == 0)
            {
                Console.WriteLine("buzz");
            }
            else if (number % 3 == 0)
            {
                Console.WriteLine("fizz");
            }
            else
            {
                Console.WriteLine("turd");
            }


        }
    }
}
