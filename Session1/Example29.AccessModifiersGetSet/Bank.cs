﻿using System;

namespace Example29.AccessModifiersGetSet
{
    public class BankAccount
    {
        private double balance;

        public double Balance
        {
            get
            {
                Console.WriteLine("Balance Check Requested");
                balance -= 1;
                Console.WriteLine($"[Balance] ${balance}");
                return balance;
            }
            set
            {
                if (value > 0)
                {
                    Console.WriteLine($"[Deposit] ${value - balance}");
                }
                else
                {
                    Console.WriteLine($"[Withdrawal] ${value}");
                    balance -= 1;
                }
                balance = value;
                Console.WriteLine($"[Balance] ${balance}");
            }
        }
    }
}
