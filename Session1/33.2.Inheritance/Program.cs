﻿using Models;
using System;
using System.Collections.Generic;

namespace InheritanceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var squareList = new List<Square>();
            squareList.Add(new Square { Side = 20});
            squareList.Add(new Square { Side = 10 });

            var circleList = new List<Circle>();
            circleList.Add(new Circle { Radius = 10});
            circleList.Add(new Circle { Radius = 20 });
            circleList.Add(new Circle { Radius = 15 });

            var rectangleList = new List<Rectangle>();
            rectangleList.Add(new Rectangle { Width = 10, Length = 100 });
            rectangleList.Add(new Rectangle { Width = 15, Length = 50 });

            Console.WriteLine("Fushat:");
            foreach (var square in squareList)
            {
                Console.WriteLine(square.GetPerimeter());
            }
            foreach (var circle in circleList)
            {
                Console.WriteLine(circle.GetPerimeter());
            }
            foreach (var rectangle in rectangleList)
            {
                Console.WriteLine(rectangle.GetPerimeter());
            }
        }
    }
}
