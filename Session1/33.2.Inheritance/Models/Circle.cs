﻿using System;

namespace Models
{
    public class Circle
    {
        public double Radius { get; set; }

        public double GetPerimeter()
        {
            return 2 * Math.PI * Radius;
        }

        public double GetArea()
        {
            return 2 * Math.PI * Radius * Radius;
        }
    }
}
