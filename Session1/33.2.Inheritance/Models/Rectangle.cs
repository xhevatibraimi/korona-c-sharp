﻿namespace Models
{
    public class Rectangle
    {
        public double Length { get; set; }
        public double Width { get; set; }

        public double GetPerimeter()
        {
            return 2 * Length + 2 * Width;
        }

        public double GetArea()
        {
            return Length * Width;
        }
    }
}
