﻿using System;

namespace _30.CalculatorExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator();

            // SUM
            //var resultIntMultipleNumbers = calculator.Sum(1, -2, -3);
            //Console.WriteLine(resultIntMultipleNumbers);

            // MULTIPLY
            //var resultIntMultipleNumbers = calculator.Multiply(1, -2, 3);
            //Console.WriteLine(resultIntMultipleNumbers);

            // POWER
            var result = calculator.Pow(2, 10);
            Console.WriteLine(result);
        }
    }
}
