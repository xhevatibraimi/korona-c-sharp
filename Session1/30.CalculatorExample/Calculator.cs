﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _30.CalculatorExample
{
    public class Calculator
    {
        public double Sum(params double[] numbers)
        {
            double sum = 0;
            foreach (var number in numbers)
            {
                sum += number;
            }
            return sum;
        }

        public double Multiply(params double[] numbers)
        {
            double product = 1;
            foreach (var number in numbers)
            {
                product *= number;
            }
            return product;
        }

        public double Pow(double number, double exponent)
        {
            // Example with For
            //double product = 1;
            //for (int i = 0; i < exponent; i++)
            //{
            //    product = product * number;
            //}
            //return product;

            // Example with While
            double product = 1;
            int i = 0;
            while (i < exponent)
            {
                product = product * number;
                i++;
            }
            return product;
        }
    }
}
