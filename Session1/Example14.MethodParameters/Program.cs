﻿using System;

namespace Example14.MethodParameters
{
    class Program
    {
        static void PrintTriangle(char character)
        {
            Console.WriteLine($"  {character}");
            Console.WriteLine($" {character}{character}{character}");
            Console.WriteLine($"{character}{character}{character}{character}{character}");
        }

        static void PrintSum(int a, int b)
        {
            Console.WriteLine(a + b);
        }

        static void PrintSubstract(int a, int b)
        {
            Console.WriteLine(a - b);
        }

        static void PrintMultiply(int a, int b)
        {
            Console.WriteLine(a * b);
        }

        static void PrintDivide(double a, double b)
        {
            Console.WriteLine(a / b);
        }

        static void Main(string[] args)
        {
            //PrintTriangle('*');
            //PrintTriangle('+');
            //PrintTriangle('-');
            //PrintTriangle('a');
            //PrintTriangle('b');
            //PrintTriangle('c');

            //PrintSum(4, 3);
            //PrintSubstract(2, 4);
            //PrintMultiply(2, 5);
            //PrintDivide(4, 3);
        }
    }
}
