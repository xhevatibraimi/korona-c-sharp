﻿namespace Models
{
    public class Triangle : Shape
    {
        public double SideA { get; set; }
        public double SideB { get; set; }
        public double SideC { get; set; }

        public override double GetPerimeter()
        {
            return SideA + SideB + SideC;
        }
    }
}
