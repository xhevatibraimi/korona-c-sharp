﻿using System;

namespace Models
{
    public class Circle : Shape
    {
        public double Radius { get; set; }

        public override double GetPerimeter()
        {
            return 2 * Math.PI * Radius;
        }
    }
}
