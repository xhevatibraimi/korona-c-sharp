﻿namespace Models
{
    public class Square : Shape
    {
        public double Side { get; set; }

        public override double GetPerimeter()
        {
            return 4 * Side;
        }
    }
}
