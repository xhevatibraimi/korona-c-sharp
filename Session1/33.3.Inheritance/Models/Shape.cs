﻿namespace Models
{
    public class Shape
    {
        public string Owner { get; set; }

        public virtual double GetPerimeter()
        {
            return 0;
        }

        public virtual double GetArea()
        {
            return 0;
        }
    }
}
