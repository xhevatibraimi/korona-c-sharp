﻿using Models;
using System;
using System.Collections.Generic;

namespace InheritanceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var shapeList = new List<Shape>();
            shapeList.Add(new Circle { Radius = 20, Owner = "haxhi musa" });
            shapeList.Add(new Square { Side = 20, Owner = "haxhi piva" });
            shapeList.Add(new Rectangle { Width = 20, Length = 15, Owner = "haxhi piva" });
            shapeList.Add(new Triangle { SideA = 3, SideB = 3, SideC = 3 });

            Console.WriteLine("Fushat:");
            foreach (var shape in shapeList)
            {
                Console.WriteLine($"Owner: {shape.Owner}, perimeter: {shape.GetPerimeter()}");
            }
        }
    }
}
