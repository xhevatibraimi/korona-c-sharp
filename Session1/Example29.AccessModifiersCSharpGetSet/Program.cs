﻿using System;

namespace Example29.AccessModifiersGetSet
{
    class Program
    {
        public static void Main(string[] args)
        {
            var bankAccount = new BankAccount();
            bankAccount.Balance += 100;
            bankAccount.Balance -= 10;
            bankAccount.Balance += 10;
            var balance = bankAccount.Balance;
            var type = typeof(BankAccount);
            var members = type.GetMembers();
            Console.WriteLine(balance);
        }
    }
}
