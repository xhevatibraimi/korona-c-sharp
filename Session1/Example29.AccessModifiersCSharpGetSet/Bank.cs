﻿namespace Example29.AccessModifiersGetSet
{
    public class BankAccount
    {
        public double Balance { get; set; }
        public string IBAN { get; set; }
    }
}
