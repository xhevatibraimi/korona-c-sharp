﻿using System;

namespace Example08.OddVsEven
{
    class Program
    {
        static void Main(string[] args)
        {
            // read the input from console
            string input = Console.ReadLine();
            // convert the string into integer
            int number = int.Parse(input);

            int mbetja = number % 2;
            if (mbetja == 0)
            {
                Console.WriteLine("chift");
            }
            else
            {
                Console.WriteLine("tek");
            }
        }
    }
}
