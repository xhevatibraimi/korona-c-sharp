﻿namespace Qoshi
{
    public class Shpendi
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsDarkMode { get; set; }
    }
}
