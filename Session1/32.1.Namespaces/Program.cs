﻿class Program
{
    public static void Main(string[] args)
    {
        var shpendKulari = new Kulari.Shpendi
        {
            Name = "shpendi",
            Age = 13,
            RememberPassword = true
        };

        var shpendQoshi = new Qoshi.Shpendi
        {
            Name = "shpendi",
            Age = 13,
            IsDarkMode = false
        };
    }
}