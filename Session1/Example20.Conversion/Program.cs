﻿using System;

namespace Example20.Conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();

            Console.WriteLine("before conversion");

            //int number2 = Int32.Parse(input);
            //bool.Parse("false");
            //Convert.ToBoolean("");
            //uint unsignedInteger32 = Convert.ToUInt32("3000000000");
            double number1 = Convert.ToDouble(input1);
            double number2 = double.Parse(input2);

            Console.WriteLine(number1 / number2);
            Console.WriteLine("after conversion");
        }
    }
}
