﻿namespace Kulari
{
    public class Shpendi
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool RememberPassword { get; set; }
    }
}