﻿using Kulari;
//using System;

class Program
{
    public static void Main(string[] args)
    {
        System.Console.WriteLine();
        
        var shpendKulari = new Shpendi
        {
            Name = "shpendi",
            Age = 13,
            RememberPassword = true
        };

        var lindiKulari = new Lindi
        {
            Name = "Lindi",
            IsPeder = true
        };

        var lindaKulari = new Kulari.Linda
        {
            Name = "Lindi",
            NekadarOrospu = int.MaxValue
        };
    }
}