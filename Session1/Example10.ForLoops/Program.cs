﻿using System;

namespace Example10.ForLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            // input
            Console.WriteLine("enter from number:");
            string fromInput = Console.ReadLine();
            int from = int.Parse(fromInput);

            Console.WriteLine("enter to number:");
            string toInput = Console.ReadLine();
            int to = int.Parse(toInput);

            // algorithm
            for (int i = from; i <= to; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
