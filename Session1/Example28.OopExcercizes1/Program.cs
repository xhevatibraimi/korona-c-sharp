﻿using System;

namespace Example28.OopExcercizes1
{
    class Program
    {
        static void Main(string[] args)
        {
            var home = new LivingPlace();
            home.Name = "shpaja";
            home.Address = new Address();
            home.Address.Number = "21/6";
            home.Address.StreetName = "bt";
            home.Address.ZipCode = "1200";
        }
    }
}
