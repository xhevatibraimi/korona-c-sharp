﻿namespace Kulari.HaramlColl
{
    public class Shpendi
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool RememberPassword { get; set; }
    }
}
namespace Kulari.HaramlColl.EnePisColl
{
    public class Shpendi
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool RememberPassword { get; set; }
        public bool IsHaramColl { get; set; }
    }
}