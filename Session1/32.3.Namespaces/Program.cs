﻿using Kulari;
using Kulari.PisColl;
using Kulari.HaramlColl;
//using System;
//using System.IO;

namespace NamespacesDemo
{
    class Program
    {
        public static void Main(string[] args)
        {
            //File.ReadAllText("c:/files/asd.txt");
            System.Console.WriteLine();

            var shpendKulari = new Shpendi
            {
                Name = "shpendi",
                Age = 13,
                RememberPassword = true
            };

            var lindiKulari = new Kulari.PisColl.Lindi
            {
                Name = "Lindi",
                IsPeder = true
            };

            var lindaKulari = new Linda
            {
                Name = "Lindi",
                NekadarOrospu = int.MaxValue
            };
        }
    }
}