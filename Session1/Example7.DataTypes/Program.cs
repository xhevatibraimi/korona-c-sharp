﻿using System;

namespace Example06.DataTypes
{
    class Program
    {
        const string url = "http://facebook.com";

        static void Main(string[] args)
        {
            // constants (const) once initialized and declared, cannot be changed
            // url = "asdsad" // -> does not compile
            // strings (string)
            string value;
            value = "";
            string name = "shdsa43567/'';;lasokj";
            string stringConcatenation = name + name;

            // integers (int)
            int number1 = 540;
            int number2 = 100;
            int sum = number1 + number2;
            double result = number1 / number2;
            // / * + - %

            // floating poing integers (float) 32bit
            float floatNumber = 1.34f;

            // double precision floating point integers (double) 64bit
            double doubleNumber = 0.1239812371983;

            doubleNumber = floatNumber;
            //floatNumber = doubleNumber; // -> does not compile

            // characters
            char someCharacter = 'b';
            //Console.WriteLine(someCharacter);

            // booleans (bool) true/false 
            bool isHungry = true;
            isHungry = false;
            bool isEven = 10 % 2 == 0;
            // 8bit value
            byte address1 = 0;
            byte[] mp3songBytes = new byte[] { 12, 2, 221, 240, 21, 2, 3, 213 };
        }
    }
}
