﻿using Models;
using System;
using System.Collections.Generic;

namespace Hospital
{
    class Program
    {
        static void Main(string[] args)
        {
            var doctorAli = new Doctor { Name = "Pr. Mr. Dr. Spec. Ali Halimi", Salary = 1000 };
            var doctorShpend = new Doctor { Name = "Pr. Mr. Dr. Spec. Shpend Kulari", Salary = 1000 };
            var nurseNenaEAliut = new Nurse { Name = "Imrane Halimi", Salary = 2000 };
            var patientXhevo = new Patient { Name = "Xhevo" };

            var people = new List<Human>();
            people.Add(doctorAli);
            people.Add(doctorShpend);
            people.Add(nurseNenaEAliut);
            people.Add(patientXhevo);
            people.Add(new Nurse { Name = "anonymous" });

            var employees = new List<IEmployee>();
            employees.Add(doctorAli);
            employees.Add(doctorShpend);
            employees.Add(nurseNenaEAliut);

            Console.WriteLine("People:");
            foreach (var person in people)
            {
                Console.WriteLine(person.Name);
            }

            Console.WriteLine("Taxes:");
            foreach (var employee in employees)
            {
                Console.WriteLine($"Tax: {employee.CalculateTax()}");
            }

        }
    }
}
