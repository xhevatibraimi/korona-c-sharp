﻿namespace Models
{
    public class Nurse : Human, IEmployee
    {
        public int Salary { get; set; }

        public double CalculateTax()
        {
            return Salary * 0.18;
        }
    }
}
