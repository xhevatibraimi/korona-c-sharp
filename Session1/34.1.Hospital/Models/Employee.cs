﻿namespace Models
{
    public class Employee
    {
        public double Salary { get; set; }

        public double CalculateTax()
        {
            return Salary * 0.18;
        }
    }
}
