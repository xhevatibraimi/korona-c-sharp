﻿namespace Models
{
    public interface IEmployee
    {
        int Salary { get; set; }
        double CalculateTax();
    }
}
