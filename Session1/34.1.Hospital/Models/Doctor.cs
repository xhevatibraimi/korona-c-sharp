﻿namespace Models
{
    public class Doctor : Human, IEmployee
    {
        public int Salary { get; set; }

        public double CalculateTax()
        {
            return Salary * 0.2;
        }
    }
}
