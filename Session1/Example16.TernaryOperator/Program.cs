﻿using System;

namespace Example16.TernaryOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (1 > 2)
            {
                Console.WriteLine(1);
            }
            else
            {
                Console.WriteLine(2);
            }

            Console.WriteLine(1 > 2 ? 1 : 2);
            Console.WriteLine('f' == 'm' ? "Male" : "Female");
            Console.WriteLine(1 == 1 || 0 > 1 ? "Male" : "Female");
        }
    }
}
