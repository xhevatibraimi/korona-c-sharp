﻿using System;

namespace Example15.MethodReturnTypes
{
    class Program
    {
        static int GetShuma(int a, int b)
        {
            int shuma = a + b;
            return shuma;
        }

        static int GetZbritja(int numri1, int numri2)
        {
            int zbritja = numri1 - numri2;
            return zbritja;
        }

        static double GetPjestimi(double numri1, double numri2)
        {
            double pjestimi = numri1 / numri2;
            return pjestimi;
        }

        static int GetProdhimi(int a, int b)
        {
            int prodhimi = a * b;
            return prodhimi;
        }

        static void Main(string[] args)
        {
            //int shuma = GetShuma(100, 50);
            //Console.WriteLine(shuma);

            //int zbritja = GetZbritja(20, 32);
            //Console.WriteLine(zbritja);   

            //double pjestimi = GetPjestimi(3, 2);
            //Console.WriteLine(pjestimi);

            //int prodhimi = GetProdhimi(20, 40);
            //Console.WriteLine(prodhimi);
        }
    }
}
