﻿using System;

namespace Example21.ThrowException
{
    class Program
    {
        static void Main(string[] args)
        {
            string input1 = Console.ReadLine();
            string input2 = Console.ReadLine();

            double number1 = Convert.ToDouble(input1);
            double number2 = double.Parse(input2);
            if (number2 == 0)
            {
                throw new Exception("Cannot divide with zero");
            }
            
            Console.WriteLine(number1 / number2);
        }
    }
}
