﻿using System;

namespace Example22.CatchExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();

            try
            {
                int number = Convert.ToInt32(input);
                Console.WriteLine(number * number);
            }
            catch(Exception error)
            {
                Console.WriteLine("exception was thrown");
                Console.WriteLine(error.Message);
            }
        }
    }
}
