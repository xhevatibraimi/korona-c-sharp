﻿using System;

namespace Example25.StaticMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Person personOne = new Person();
            personOne.Name = "XHEVO";
            personOne.Age = 25;
            personOne.Address = new Address();
            personOne.Address.City = "tetovo";
            personOne.Address.GeoLocation = new Location();
            personOne.Address.GeoLocation.Latitude = 42.00;
            personOne.Address.GeoLocation.Longitude = 20.97;
            PrintPerson(personOne);
        }

        static void PrintPerson(Person person)
        {
            Console.WriteLine("Name: " + person.Name);
            Console.WriteLine("Age: " + person.Age);
            Console.WriteLine("City: " + person.Address.City);
            Console.WriteLine($"Location: ({person.Address.GeoLocation.Latitude},{person.Address.GeoLocation.Longitude})");
        }
    }

    class Person
    {
        public Address Address;
        public string Name;
        public int Age;
    }

    class Address
    {
        public string City;
        public Location GeoLocation;
        private string ZipCode;
    }

    class Location
    {
        public double Latitude;
        public double Longitude;
    }
}
