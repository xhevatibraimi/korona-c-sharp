﻿using System;

namespace Example25.Classes
{
    public class Student
    {
        public string Name { get; set; }
        public string IndexNumber { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string[] Courses { get; set; }
        public string Faculty { get; set; }
        public string Module { get; set; }

        public void Print()
        {
            Console.WriteLine($"#{IndexNumber} {Name}({Age}/{Gender}), {Faculty}-{Module}");
            Console.WriteLine("courses:");
            foreach (var course in Courses)
            {
                Console.WriteLine($"* {course}");
            }
        }
    }
}
