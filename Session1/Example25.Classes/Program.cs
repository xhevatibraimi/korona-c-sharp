﻿using System;

namespace Example25.Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student();
            s.Age = 25;
            s.Faculty = "CS";
            s.Gender = "male";
            s.IndexNumber = "10001";
            s.Name = "xhevo";
            s.Module = "business informatics";
            s.Courses = new string[] { "math", "intro in CS", "programming" };

            s.Print();
        }
    }
}
