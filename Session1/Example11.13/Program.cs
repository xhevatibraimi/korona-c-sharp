﻿using System;

namespace Example13.Methods
{

    class Program
    {
        static void PrintTriangle()
        {
            Console.WriteLine("   *");
            Console.WriteLine("  ***");
            Console.WriteLine(" *****");
            Console.WriteLine("*******");
        }

        static void PrintSquare()
        {
            Console.WriteLine("*******");
            Console.WriteLine("*******");
            Console.WriteLine("*******");
            Console.WriteLine("*******");
        }

        static void PrintRocket()
        {
            PrintTriangle();
            PrintSquare();
            PrintSquare();
        }

        static void PrintTree()
        {
            PrintTriangle();
            PrintTriangle();
            PrintTriangle();
        }

        static void Main(string[] args)
        {
            PrintRocket();
            Console.WriteLine();
            PrintTree();
        }
    }
}
