﻿using System;

namespace Example17.BreakStatement
{
    class Program
    {
        static void PrintName()
        {
            Console.WriteLine("xhevo");
        }
        static void Main(string[] args)
        {
            //for (int i = 1; i < 100; i++)
            //{
            //    if (i % 2.5 == 0)
            //    {
            //        Console.WriteLine(i);
            //        break;
            //    }
            //}

            //int i = 1;
            //while (i <= 10)
            //{
            //    if (i % 3 == 0)
            //    {
            //        Console.WriteLine(i);
            //        break;
            //    }
            //    i++;

            //    //    //i = i + 1;
            //    //    //i += 1;

            //    //    //i = i + 5;
            //    //    //i += 5;
            //}

            int i = 1;
            do
            {
                if (i % 4 == 0)
                {
                    Console.WriteLine(i);
                    break;
                }
                i++;
            }
            while (i <= 10);
        }
    }
}
