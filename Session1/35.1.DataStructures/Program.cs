﻿using System;
using System.Collections.Generic;

namespace DataStructures
{
    class Program
    {
        static void Main(string[] args)
        {
            // var person = new Person { Age = 20 };
            // UpdateAge(person);
            // Console.WriteLine(person.Age);

            // var animal = new Animal { Age = 2 };
            // UpdateAnimal(animal);
            // Console.WriteLine(animal.Age);

            //var number = 100;
            //Console.WriteLine(number);
            //number = ChangeNumberAndReturnIt(number);
            //Console.WriteLine(number);

            // var numbers = new int[40];
            // numbers[0] = 123;
            // Console.WriteLine(numbers[6]);

            //var numbersList = new List<int> { 1, 2, 38, 45, 5, 5, 5, 4, 4 };
            //var numbersList = new List<int>();
            //Console.WriteLine(numbersList.Count);
            //Console.WriteLine(numbersList.Capacity);
            //Console.WriteLine(numbersList[1]);

            //var numbersQueue = new Queue<int>();
            //numbersQueue.Enqueue(5);
            //numbersQueue.Enqueue(2);
            //numbersQueue.Enqueue(4);
            //Console.WriteLine(numbersQueue.Dequeue());

            //var numbersStack = new Stack<int>();
            //numbersStack.Push(3);
            //numbersStack.Push(9);
            //numbersStack.Push(6);
            //Console.WriteLine(numbersStack.Pop());
            //Console.WriteLine(numbersStack.Pop());
            //Console.WriteLine(numbersStack.Pop());

            var students = new Dictionary<string, int>();
            students.Add("ali halimi", 5);
            students.Add("ali zaimi", 6);
            students.Add("ali hoxha", 7);

            Console.WriteLine(students["ali halimi"]);
            foreach (var student in students)
            {
                Console.WriteLine($"{student.Key}: {student.Value}");
            }
        }

        static void UpdateAnimal(Animal a)
        {
            a.Age = 500;
        }

        static void UpdateAge(Person p)
        {
            p.Age = 100;
        }

        static void ChangeNumber(int number)
        {
            number = 200;
        }

        static int ChangeNumberAndReturnIt(int number)
        {
            number = 200;
            return number;
        }

        static void PrintArray(int[] collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }
    }
}
