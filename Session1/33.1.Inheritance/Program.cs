﻿using Models;
using System;

namespace InheritanceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var square = new Square();
            square.Side = 20;

            var perimeter = square.GetPerimeter();
            var area = square.GetArea();

            //Console.WriteLine($"Perimeter: {perimeter}, Area: {area}");

            var rectangle = new Rectangle();
            rectangle.Length = 15;
            rectangle.Width = 10;

            var rectanglePerimeter = rectangle.GetPerimeter();
            var rectangleArea = rectangle.GetArea();

            //Console.WriteLine($"[Rectangle] Perimeter: {rectanglePerimeter}, Area: {rectangleArea}");

            var circle = new Circle();
            circle.Radius = 5;
            var circlePerimeter = circle.GetPerimeter();
            var circleArea = circle.GetArea();

            Console.WriteLine($"[Circle] Perimeter: {circlePerimeter}, Area: {circleArea}");


        }
    }
}
