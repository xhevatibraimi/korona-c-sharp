﻿namespace Models
{
    public class Square
    {
        public double Side { get; set; }

        public double GetPerimeter()
        {
            return 4 * Side;
        }

        public double GetArea()
        {
            return Side * Side;
        }
    }
}
