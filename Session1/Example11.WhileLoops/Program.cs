﻿
using System;

namespace Example11.WhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 30;
            while (number <= 35)
            {
                Console.WriteLine(number);
                number++;
            }
        }
    }
}
