﻿namespace Example28.OopExcercizes2
{
    public class Planet
    {
        public string Name;
        public int Diameter;

        public Planet()
        {
        }

        public Planet(string name, int diameter)
        {
            Name = name;
            Diameter = diameter;
        }
    }
}
