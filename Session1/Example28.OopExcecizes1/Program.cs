﻿using System;

namespace Example28.OopExcercizes2
{
    class Program
    {
        static void Main(string[] args)
        {
            //var human1 = new Human();
            //human1.FirstName = "ali";
            //human1.LastName = "halimi";
            //human1.Car = new Automobile();
            //human1.Car.Make = "bmw";
            //human1.Car.Model = "320";
            //human1.Car.ProductionYear = 2006;
            //human1.Car.PowerKW = 120;
            //human1.Car.PowerPS = 163;

            //var outputMessage = $"{human1.FirstName} {human1.LastName}, {human1.Car.Make}";
            //Console.WriteLine(outputMessage);

            // Initializing objects Method #1
            //var lp = new LivingPlace();
            //lp.Address = "blagoja toska";
            //lp.City = new City();
            //lp.City.Name = "tetovo";
            //lp.City.Country = new Country();
            //lp.City.Country.Name = "nmk";
            //lp.City.Country.Continent = new Continent();
            //lp.City.Country.Continent.Name = "Europe";
            //lp.City.Country.Continent.Planet = new Planet();
            //lp.City.Country.Continent.Planet.Name = "earth";
            //lp.City.Country.Continent.Planet.Diameter = 12000;

            // Initializing objects Method #2
            //var planetEarth = new Planet();
            //planetEarth.Name = "earth";
            //planetEarth.Diameter = 12000;

            //var continent = new Continent();
            //continent.Name = "Europe";
            //continent.Planet = planetEarth;

            //var nmk = new Country();
            //nmk.Name = "NMK";
            //nmk.Continent = continent;

            //var tetovoCity = new City();
            //tetovoCity.Name = "Tetovo";
            //tetovoCity.Country = nmk;

            //var livingPlace = new LivingPlace();
            //livingPlace.Address = "blagoja toska";
            //livingPlace.City = tetovoCity;

            // Initializing objects Method #3
            //var lp = new LivingPlace
            //{
            //    Address = "bt",
            //    City = new City
            //    {
            //        Name = "tetovo",
            //        Country = new Country
            //        {
            //            Name = "nmk",
            //            Continent = new Continent
            //            {
            //                Name = "Europe",
            //                Planet = new Planet
            //                {
            //                    Name = "Earth",
            //                    Diameter = 12000
            //                }
            //            }
            //        }
            //    }
            //};


            //Initializing objects Method #4
            //var planetEarth = new Planet();
            //planetEarth.Name = "earth";
            //planetEarth.Diameter = 12000;

            //var lp = new LivingPlace
            //{
            //    Address = "bt",
            //    City = new City
            //    {
            //        Name = "tetovo",
            //        Country = new Country
            //        {
            //            Name = "nmk",
            //            Continent = new Continent
            //            {
            //                Name = "Europe",
            //                Planet = planetEarth
            //            }
            //        }
            //    }
            //};

            // Initializing objects Method #5
            var planetMars = new Planet();
            planetMars.Name = "Mars";
            planetMars.Diameter = 6000;

            var planetEarth = new Planet("Earth", 12000);
            var europeContinent = new Continent("Europe", planetEarth);
            var countryNMK = new Country("NMK", europeContinent);
            var tetovoCity = new City("Tetovo", countryNMK);
            var livingPlace = new LivingPlace("bt", tetovoCity);
            var livingPlaceNamedParameters = new LivingPlace(city: tetovoCity, address: "bt");
        }
    }
}
