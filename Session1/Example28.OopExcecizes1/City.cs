﻿namespace Example28.OopExcercizes2
{
    public class City
    {
        public string Name;
        public Country Country;

        public City()
        {

        }

        public City(string name, Country country)
        {
            Name = name;
            Country = country;
        }
    }
}
