﻿namespace Example28.OopExcercizes2
{
    public class LivingPlace
    {
        public string Address;
        public City City;

        public LivingPlace()
        {

        }

        public LivingPlace(string address, City city)
        {
            Address = address;
            City = city;
        }
    }
}
