﻿namespace Example28.OopExcercizes2
{
    public class Automobile
    {
        public string Make;
        public string Model;
        public int ProductionYear;
        public int PowerKW;
        public int PowerPS;
    }
}
