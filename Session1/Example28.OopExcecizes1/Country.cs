﻿namespace Example28.OopExcercizes2
{
    public class Country
    {
        public string Name;
        public Continent Continent;

        public Country() { }

        public Country(string name, Continent continent)
        {
            Name = name;
            Continent = continent;
        }
    }
}
