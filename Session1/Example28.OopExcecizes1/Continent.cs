﻿namespace Example28.OopExcercizes2
{
    public class Continent
    {
        public string Name;
        public Planet Planet;

        public Continent()
        {

        }

        public Continent(string name, Planet planet)
        {
            Name = name;
            Planet = planet;
        }
    }
}
