﻿using System;

namespace Example24.Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of tables:");
            int numberOfTables = int.Parse(Console.ReadLine());
            
            int[] tables = new int[numberOfTables];

            for (int i = 0; i < numberOfTables; i++)
            {
                Console.WriteLine($"insert table {i + 1}:");
                tables[i] = Convert.ToInt32(Console.ReadLine());
            }

            int shuma = 0;

            foreach (int table in tables)
            {
                shuma = shuma + table;
            }
            
            Console.WriteLine(shuma );
        }
    }
}
