﻿using System;

namespace Example27.OopPrinciples
{
    class Program
    {
        // #1
        //static void Main(string[] args)
        //{
        //    var firstNamePerson1 = "xhevo";
        //    var lastNamePerson1 = "ibr";
        //    var firstNamePerson2 = "ali";
        //    var lastNamePerson2 = "halimi";
        //    Print(firstNamePerson1, lastNamePerson1);
        //    Print(firstNamePerson2, lastNamePerson2);
        //}

        // #2
        //static void Main(string[] args)
        //{
        //    var human1 = new Human();
        //    human1.FirstName = "xhevo";
        //    human1.LastName = "ibr";

        //    var human2 = new Human();
        //    human2.FirstName = "ali";
        //    human2.LastName = "halimi";

        //    Print(human1);
        //    Print(human2);
        //}

        static public void Print(Human h)
        {
            Console.WriteLine($"F: {h.FirstName}, L: {h.LastName}");
        }

        static public void Print(string first, string last)
        {
            Console.WriteLine($"F: {first}, L: {last}");
        }
    }

    public class Human
    {
        public string FirstName;
        public string LastName;
    }
}
