﻿using System;

namespace Example18.SumCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int shuma = 0;
            for (int i = 0; i <= 100; i++)
            {
                shuma = shuma + i;
                //Console.WriteLine($"i: {i}, shuma: {shuma}");
            }

            Console.WriteLine(shuma);

            //1 2 3 .... 98  99  100
            //^ ^ ^       ^   ^   ^
            //| | \--101--/   |   |
            //| \----101------/   |
            //\------101----------/
            // 100 / 2 = 50
            // 100 + 1 = 101
            // shuma  = 101 * 50
        }
    }
}
