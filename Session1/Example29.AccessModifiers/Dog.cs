﻿using System;

namespace Example29.AccessModifiers
{
    public class Dog
    {
        private string Name;
        private string Race;
        private int Years;

        public string GetName()
        {
            return Name;
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetRace(string Race)
        {
            this.Race = Race;
        }

        public void SetYears(int years)
        {
            Years = years;
        }

        public void Print()
        {
            Console.WriteLine($"[{Race}]{Name}({Years})");
        }
    }
}
