﻿using System;

namespace Example29.AccessModifiers
{
    class Program
    {
        public static void Main(string[] args)
        {
            var pitbull = new Dog();
            pitbull.SetName("xhoni");
            pitbull.SetRace("pitbull");
            pitbull.SetYears(2);            
            pitbull.Print();
        }
    }
}
